<?php

class UserController extends Controller
{
	
    public function actionIndex(){
        $users = new CActiveDataProvider('Users',array(
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));
        $this->render('index', array(
            'users' => $users
        ));
    }

    public function actionCreate(){
        $model = new Users();
        if(isset($_POST['Users'])){
            $model->username = $_POST['Users']['username'];
            $model->password = $model->hashPassword($_POST['Users']['username'], $_POST['Users']['password']);
                if($model->save()){
                        $this->redirect(array('/user/index'));
                    }
            }
        $this->render('create',['model'=>$model]);
    }
    public function actionDelete($ids){
            $pkIds = explode(",", $ids);
            foreach($pkIds as $id){
                    $task = Users::model()->findByPk($id);
                    $task->delete();
            }
    }
}
<?php

class TodoController extends Controller
{
    public function actionIndex(){
        $todo = new CActiveDataProvider('Todo',array(
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));
        $this->render('index', array(
            'todo' => $todo
        ));
    }

    public function actionCreate(){
        $model = new Todo();
        if(isset($_POST['Todo'])){
                $model->attributes = $_POST['Todo'];
                if($model->save()){
                        $this->redirect(array('/todo/index'));
                    }
            }
        $this->render('create',['model'=>$model]);
    }
    public function actionSetComplete($ids){
            $pkIds = explode(",", $ids);
            foreach($pkIds as $id){
                    $task = Todo::model()->findByPk($id);
                    $task->completed = 1;
                    $task->completedOn = date("Y-m-d");
                    $task->update();
            }
    }
    public function actionDelete($ids){
            $pkIds = explode(",", $ids);
            foreach($pkIds as $id){
                    $task = Todo::model()->findByPk($id);
                    $task->delete();
            }
    }
}
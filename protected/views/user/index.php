<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Users</h1>
<p><a href="<?php echo $this->createUrl('/user/create');?>">Create New user</a></p>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'all-user-grid',
	'dataProvider'=>$users,
	'summaryText' => '<button id="delete-users">Delete User</button>&nbsp;',
	'selectableRows'=>0,
	'columns'=>array(
		array(
			'id'=>'id',
			'class'=>'CCheckBoxColumn',
			'selectableRows' => '50',
		),
		array(
			'name' => 'username',
			'htmlOptions'=> array('align'=>'center'),
		),


	),
)); ?>

<?php

Yii::app()->clientScript->registerScript('js',"
	$(document).ready(function(){
	
		$('#delete-users').on('click',function(){
			var val = [];
			$('input[name=\"id[]\"]:checked:enabled').each(function(i){
					val[i] = $(this).val();
			});
			if(val.length == 0){
					alert('Please select at least one record!');
					return false;
			}else {
					var c = confirm('Are you sure you want delete these');
					if( c ){
							var ids  = 'ids/'+val.join(',');
							$.get('".Yii::app()->createUrl("user/delete")."/'+ids)
									.done(function(){
										location.reload()
											/* $.fn.yiiGridView.update('all-my-tasks-grid', {
													data: $(this).serialize()
											}); */
									});
					}
			}
			return false;
		});
	});
	
	
");

?>
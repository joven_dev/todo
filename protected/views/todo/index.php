<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Todo</h1>
<p><a href="<?php echo $this->createUrl('/todo/create');?>">Create New tasks</a></p>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'all-my-tasks-grid',
	'dataProvider'=>$todo,
	'summaryText' => '<button id="mark-complete">Mark Completed</button>&nbsp;<button id="delete-tasks">Delete Tasks</button>&nbsp;',
	'selectableRows'=>0,
	'columns'=>array(
		array(
			'id'=>'id',
			'class'=>'CCheckBoxColumn',
			'selectableRows' => '50',
		),
		array(
			'name' => 'title',
			'htmlOptions'=> array('align'=>'center'),
		),
		array(
			'name' => 'description',

		),
		array(
			'name' => 'Due Date',
			'value' => '$data->dueOn'
		),
		array(
			'name' => 'Completed',
			'value' => '$data->completed ? "true" : "false"'
		),


	),
)); ?>

<?php

Yii::app()->clientScript->registerScript('js',"
	$(document).ready(function(){
		$('#mark-complete').on('click',function(){
			console.log('test');
				var val = [];
				$('input[name=\"id[]\"]:checked:enabled').each(function(i){
						val[i] = $(this).val();
				});
				if(val.length == 0){
						alert('Please select at least one record!');
						return false;
				}else {
						var c = confirm('Are you sure you want set these as completed.');
						if( c ){
								var ids  = 'ids/'+val.join(',');
								$.get('".Yii::app()->createUrl("todo/setcomplete")."/'+ids)
										.done(function(){
											location.reload()
												/* $.fn.yiiGridView.update('all-my-tasks-grid', {
														data: $(this).serialize()
												}); */
										});
						}
				}
				return false;
		});
	
		$('#delete-tasks').on('click',function(){
			var val = [];
			$('input[name=\"id[]\"]:checked:enabled').each(function(i){
					val[i] = $(this).val();
			});
			if(val.length == 0){
					alert('Please select at least one record!');
					return false;
			}else {
					var c = confirm('Are you sure you want delete these');
					if( c ){
							var ids  = 'ids/'+val.join(',');
							$.get('".Yii::app()->createUrl("todo/delete")."/'+ids)
									.done(function(){
										location.reload()
											/* $.fn.yiiGridView.update('all-my-tasks-grid', {
													data: $(this).serialize()
											}); */
									});
					}
			}
			return false;
		});
	});
	
	
");

?>
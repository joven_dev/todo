<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	private $jwtCookie;
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public function init()
	{
		try
		{
			if($this->getJwtCookie())
			{
				$payload = $this->getJwtPayload();

				$account = $this->getJwtAccountData($payload);

				$account->autoLogin();
			}
		}
		catch (Exception $e)
		{
			$e->getMessage();
		}
	}

	public function getJwtCookie()
	{
		if (isset($_COOKIE['auth_jwt']) && $_COOKIE['auth_jwt'] != '') {
			$this->jwtCookie = $_COOKIE['auth_jwt'];
			return true;
		}

		return false;
	}

	public function getJwtPayload()
	{
		return json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $this->jwtCookie)[1]))));
	}

	protected function getJwtAccountData($payload)
	{
		if (!isset($payload->account_id) || $payload->account_id == '')
			throw new Exception('Missing payload account id!');

		if (!isset($payload->company_id) || $payload->company_id == '')
			throw new Exception('Missing payload company id!');

		$user = Users::model()->findByPk($payload->account_id);

		if (!$user)
			throw new Exception('Invalid payload account!');

		return $user;
	}
}
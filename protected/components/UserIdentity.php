<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($autoLogin = false)
	{
		$username=strtolower($this->username);
		$account=Users::model()->find('LOWER(username)=?',array($username));
		
		if ($account === null) 
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if(!$autoLogin && !$account->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
        {
            $this->_id=$account->id;
            $this->username=$account->username;
			Yii::app()->user->setState('username', $account->username);
			Yii::app()->user->setState('id', $account->id);
            $this->errorCode=self::ERROR_NONE;;
        }
        return $this->errorCode==self::ERROR_NONE;
	}
}
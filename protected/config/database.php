<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=todo-mysql;dbname=todo;port=3306',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'password123',
	'charset' => 'utf8',
	'tablePrefix' => 'alt_'  
);